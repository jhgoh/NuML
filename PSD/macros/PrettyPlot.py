#!/usr/bin/env python
import ROOT
import yaml
import os
import copy

class PrettyPlot:
    def __init__(self, **kwargs):
        self.objs = []

        ## The default configuration
        self.config = {
            'canvas':{
                'height':400, 'width':400,
                'margin-left':80, 'margin-right':20, 'margin-top':50, 'margin-bottom':80,
            },
            'x-axis':{'offset':1.30, 'font-size':0.040, 'title':''},
            'y-axis':{'offset':1.50, 'font-size':0.045, 'title':''},
            'header':{
                'image':'Logo_JSNS2_v2_crop.png',
                ## input image size (could be ommitted in the future version)
                'image-height':180, 'image-width':625,
                'height':30, 'width':None,
                ## relative position w.r.t the upper edge of the top-axis
                ## give one of left/right and top/bottom then the other edges
                ## will be automatically done based on height or width
                'margin-top':None, 'margin-bottom':5,
                'margin-left':None, 'margin-right':0,
                #'margin-left':0, 'margin-right':None,
                #'margin-top':10, 'margin-bottom':None,
            },
        }

        if 'configFile' in kwargs:
            self.loadConfig(kwargs['configFile'])

        if 'configToOverride' in kwargs:
            self.config = PrettyPlot.overrideConfig(self.config, kwargs['configToOverride'])

        self.applyDefaultStyle()

    def loadConfig(self, configFileName):
        if not os.path.exists(configFile):
            print("@@@ Cannot find config file '%s'" % (configFileName))
            return

        config = yaml.load(open(configFileName), Loader=yaml.Loader)
        self.config = PrettyPlot.overrideConfig(self.config, config)

    def overrideConfig(d1, d2):
        d2 = PrettyPlot.processConfigItems(d2)
        d = copy.deepcopy(d1)
        for k, v in d2.items():
            if k not in d or type(v) != dict: d[k] = v
            else: d[k] = PrettyPlot.overrideConfig(d[k], v)
        return d

    def processConfigItems(din):
        dout = {}
        for k, v in din.items():
            k = k.strip('/')
            if k == '': continue ## this should not happen

            ddout = dout
            ks = k.split('/')
            for kk in ks[:-1]:
                if kk not in ddout:
                    ddout[kk] = {}
                ddout = ddout[kk]
            ddout[ks[-1]] = v
        return dout

    def applyDefaultStyle(self):
        cc = self.config['canvas']
        w, h = cc['width'], cc['height']
        l, r = cc['margin-left'], cc['margin-right']
        t, b = cc['margin-top'], cc['margin-bottom']
        cW, cH = w+l+r, h+t+b

        ROOT.gStyle.SetCanvasDefH(cH)
        ROOT.gStyle.SetCanvasDefW(cW)
        ROOT.gStyle.SetPadLeftMargin(l/cW)
        ROOT.gStyle.SetPadRightMargin(r/cW)
        ROOT.gStyle.SetPadTopMargin(t/cH)
        ROOT.gStyle.SetPadBottomMargin(b/cH)

        ROOT.gStyle.SetTitleXOffset(self.config['x-axis']['offset'])
        ROOT.gStyle.SetTitleXSize(self.config['x-axis']['font-size'])
        ROOT.gStyle.SetTitleYOffset(self.config['y-axis']['offset'])
        ROOT.gStyle.SetTitleYSize(self.config['y-axis']['font-size'])

    def drawOverlay(self, prefix, hists, titles, config):
        config = PrettyPlot.overrideConfig(self.config, config)

        ccfg = config['canvas']
        w, h = ccfg['width'], ccfg['height']
        l, r = ccfg['margin-left'], ccfg['margin-right']
        t, b = ccfg['margin-top'], ccfg['margin-bottom']
        cW, cH = w+l+r, h+t+b

        hcfg = config['header']
        imW, imH = hcfg['image-width'], hcfg['image-height']
        hW, hH = hcfg['width'], hcfg['height']
        if hW == None: hW = hH/imH*imW
        if hH == None: hH = hW/imW*imH
        hL, hR = hcfg['margin-left'], hcfg['margin-right']
        hT, hB = hcfg['margin-top'], hcfg['margin-bottom']

        xtitle = config['x-axis']['title']
        ytitle = config['y-axis']['title']

        self.canvas = ROOT.TCanvas("c"+prefix, "c"+prefix, cW, cH)
        #self.canvas.SetCanvasSize(2*cW, cH)
        #self.canvas.SetLogy()
        self.canvas.SetGridx()
        self.canvas.SetGridy()

        pt = ROOT.gStyle.GetPadTopMargin()
        pl = ROOT.gStyle.GetPadLeftMargin()
        pr = ROOT.gStyle.GetPadRightMargin()

        self.legend = ROOT.TLegend(pl+0.05, 1-pt-0.03-0.07*len(hists), pl+0.05+0.4, 1-pt-0.03)
        self.legend.SetBorderSize(0)
        self.legend.SetFillStyle(0)

        self.hstack = ROOT.THStack('hs'+prefix, "%s;%s;%s" % (prefix, xtitle, ytitle))
        for hist in hists:
            self.hstack.Add(hist)
        for hist, title in zip(reversed(hists), reversed(titles)):
            self.legend.AddEntry(hist, title)

        hL1 = 0
        if hL != None:
            hL = l+hL
            hR = hL+hW
            hL1 = hR+hW*0.05
        else:
            hR = l+w-hR
            hL = hR-hW
            hL1 = l
        if hB != None:
            hB = b+h+hB
            hT = hB+hH
        else:
            hT = b+h-hT
            hB = hT-hH

        #canvas.SetCanvasSize(cW, cH)
        #canvas.SetWindowSize(cW + (cW - canvas.GetWw())+90, cH + (cH - canvas.GetWh())+20)
        #canvas.SetWindowSize(canvas.GetWw(), canvas.GetWh())
        #canvas.SetMargin(l, r, b, t)
        #self.canvas.SetTitleXOffset(config['x-axis']['offset'])
        #self.canvas.SetTitleXSize(config['x-axis']['font-size'])
        #self.canvas.SetTitleYOffset(config['y-axis']['offset'])
        #self.canvas.SetTitleYSize(config['y-axis']['font-size'])

        self.hstack.Draw("nostack,hist")
        self.legend.Draw()
        if 'label' in hcfg and hcfg['label'] != "":
            self.canvas.cd()
            pHeader = ROOT.TPad("pHeader_"+self.canvas.GetName(), "", hL1/cW, hB/cH, 1, hT/cH)
            pHeader.Draw()
            pHeader.cd()
            label = ROOT.TLatex(0,0, config['header']['label'])
            label.SetTextSize(0.8)
            label.SetTextAlign(10)
            label.Draw()
            self.objs.append(label)
            pHeader.SetEditable(False)

        if 'image' in hcfg and hcfg['image'] not in (None, ''):
            self.canvas.cd()
            pLogo = ROOT.TPad("pLogo_"+self.canvas.GetName(), "", hL/cW, hB/cH, hR/cW, hT/cH)
            pLogo.Draw()
            pLogo.cd()
            logo = ROOT.TImage.Open(config['header']['image'])
            logo.SetImageQuality(ROOT.TAttImage.kImgBest)
            logo.Draw("xxx")
            logo.SetEditable(False)
            pLogo.SetEditable(False)

    def drawSideBySide(self, prefix, hists, titles, config):
        config = PrettyPlot.overrideConfig(self.config, config)
        option = config['plot']['option']

        ccfg = config['canvas']
        w, h = ccfg['width'], ccfg['height']
        l, r = ccfg['margin-left'], ccfg['margin-right']
        t, b = ccfg['margin-top'], ccfg['margin-bottom']
        cW, cH = w+l+r, h+t+b

        hcfg = config['header']
        imW, imH = hcfg['image-width'], hcfg['image-height']
        hW, hH = hcfg['width'], hcfg['height']
        if hW == None: hW = hH/imH*imW
        if hH == None: hH = hW/imW*imH
        hL, hR = hcfg['margin-left'], hcfg['margin-right']
        hT, hB = hcfg['margin-top'], hcfg['margin-bottom']

        xtitle = config['x-axis']['title']
        ytitle = config['y-axis']['title']

        self.canvas = ROOT.TCanvas("c"+prefix, "c"+prefix, 2*cW, cH)
        self.canvas.Divide(len(hists))

        for i, hist in enumerate(hists):
            pad = self.canvas.cd(i+1)
            hist.GetXaxis().SetTitle(xtitle)
            hist.GetYaxis().SetTitle(ytitle)
            hist.Draw(option)

            #pad.SetLogy()
            pad.SetGridx()
            pad.SetGridy()

        hL1 = 0
        if hL != None:
            hL = l+hL
            hR = hL+hW
            hL1 = hR+hW*0.05
        else:
            hR = l+w-hR
            hL = hR-hW
            hL1 = l
        if hB != None:
            hB = b+h+hB
            hT = hB+hH
        else:
            hT = b+h-hT
            hB = hT-hH

        #canvas.SetCanvasSize(cW, cH)
        #canvas.SetWindowSize(cW + (cW - canvas.GetWw())+90, cH + (cH - canvas.GetWh())+20)
        #canvas.SetWindowSize(canvas.GetWw(), canvas.GetWh())
        #canvas.SetMargin(l, r, b, t)
        #canvas.SetTitleXOffset(config['x-axis']['offset'])
        #canvas.SetTitleXSize(config['x-axis']['font-size'])
        #canvas.SetTitleYOffset(config['y-axis']['offset'])
        #canvas.SetTitleYSize(config['y-axis']['font-size'])

        for i, (hist, title) in enumerate(zip(hists, titles)):
            pad = self.canvas.cd(i+1)
            if 'label' in hcfg and hcfg['label'] != "":
                pHeader = ROOT.TPad("pHeader_"+pad.GetName(), "", hL1/cW, hB/cH, 1, hT/cH)
                pHeader.Draw()
                pHeader.cd()
                label = ROOT.TLatex(0,0, config['header']['label'])
                label.SetTextSize(0.8)
                label.SetTextAlign(10)
                label.Draw()
                self.objs.append(label)
                pHeader.SetEditable(False)

            pad = self.canvas.cd(i+1)
            #label2 = ROOT.TLatex(hL1/cW, hB/cH, title)
            label2 = ROOT.TLatex()
            label2.SetTextAlign(13)
            label2.SetTextFont(42)
            label2.DrawLatexNDC(l/cW+0.05, 1-t/cH-0.02, title)
            self.objs.append(label2)

            if 'image' in hcfg and hcfg['image'] not in (None, ''):
                pLogo = ROOT.TPad("pLogo_"+pad.GetName(), "", hL/cW, hB/cH, hR/cW, hT/cH)
                pLogo.Draw()
                pLogo.cd()
                logo = ROOT.TImage.Open(hcfg['image'])
                logo.SetImageQuality(ROOT.TAttImage.kImgBest)
                logo.Draw("xxx")
                logo.SetEditable(False)
                pLogo.SetEditable(False)
