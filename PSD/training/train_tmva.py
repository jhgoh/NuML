#!/usr/bin/env python
from ROOT import *

TMVA.Tools.Instance()

fin = TFile("template.root")
tree = fin.Get("tree")

dl = TMVA.DataLoader("dataset")
for i in range(2480//(5*16)): dl.AddVariable("wf%d"%i)
dl.AddSignalTree(tree)
dl.AddBackgroundTree(tree)
dl.PrepareTrainingAndTestTree(TCut("label==0"), TCut("label==1"), "SplitMode=Random")

fout = TFile("tmva.root", "recreate")

factory = TMVA.Factory("mva", fout, "V:!Silent:Color:Transformations=I:DrawProgressBar:AnalysisType=Classification")

#factory.BookMethod(dl, TMVA.Types.kLikelihood, "Likelihood",
#                   "!H:!V:!TransformOutput:PDFInterpol=Spline2"
#                   ":NSmoothSig[0]=20:NSmoothBkg[0]=10:NSmooth=1"
#                   ":NAvEvtPerBin=50")
#factory.BookMethod(dl, TMVA.Types.kLikelihood, "LikelihoodD",
#                   "!H:!V:!TransformOutput:PDFInterpol=Spline2"
#                   ":NSmoothSig[0]=20:NSmoothBkg[0]=20:NSmooth=5"
#                   ":NAvEvtPerBin=50:VarTransform=Decorrelate")
factory.BookMethod(dl, TMVA.Types.kMLP, "MLP",
                   "H:!V:NeuronType=tanh:VarTransform=N:NCycles=600"
                   ":HiddenLayers=N+5:TestRate=5:!UseRegulator")
factory.BookMethod(dl, TMVA.Types.kBDT, "BDT",
                   "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3"
                   ":BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost"
                   ":BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20")
factory.BookMethod(dl, TMVA.Types.kBDT, "BDTG",
                   "!H:!V:NTrees=1000:MinNodeSize=2.5%"
                   ":BoostType=Grad:Shrinkage=0.10:UseBaggedBoost"
                   ":BaggedSampleFraction=0.5:nCuts=20:MaxDepth=2")
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

fout.Close()

TMVA.TMVAGui("tmva.root")
