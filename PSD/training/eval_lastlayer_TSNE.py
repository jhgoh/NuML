#!/usr/bin/env python
import sys, os
import argparse
import numpy as np
import pandas as pd
import uproot
import csv
import h5py
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import yaml




from dataset.WFChDataset import *  ## need to change each code script
path = ''   ## model / weight file path
configfile = ''   ## config file name


sys.path.append("./python")
model = torch.load(path + '/model.pth', map_location='cpu')
model.load_state_dict(torch.load(path + '/weight.pth', map_location='cpu'))



config = yaml.load(open(configfile).read(), Loader=yaml.FullLoader)
config['training']['learningRate'] = float(config['training']['learningRate'])


dset = WFCh2Dataset(channel=config['format']['channel'])


for sampleInfo in config['samples']:
    if 'ignore' in sampleInfo and sampleInfo['ignore']: continue
    name = sampleInfo['name']
    dset.addSample(name, sampleInfo['path'], weight=sampleInfo['xsec']/sampleInfo['ngen'])
    dset.setProcessLabel(name, sampleInfo['label'])
dset.initialize()
lengths = [int(x*len(dset)) for x in config['training']['splitFractions']]
lengths.append(len(dset)-sum(lengths))
torch.manual_seed(config['training']['randomSeed1'])
trnDset, valDset, testDset = torch.utils.data.random_split(dset, lengths)
kwargs = {'num_workers':min(config['training']['nDataLoaders'], os.cpu_count()), 'pin_memory':False}
from torch.utils.data import DataLoader
trnLoader = DataLoader(trnDset, batch_size=32, shuffle=True, **kwargs)
valLoader = DataLoader(valDset, batch_size=32, shuffle=False, **kwargs)

torch.manual_seed(torch.initial_seed())
testLoader = DataLoader(testDset, batch_size=32, shuffle=False, **kwargs)



## detach model before 'fc'
fea512 = nn.Sequential(*(list(model.children())[:-1]))

## detach model 'fc' remove last layer
fc1024t512 = nn.Sequential(*[model.fc[:-2]])



out = np.zeros((0,512))
lab = np.zeros((0))

from tqdm import tqdm
for epoch in range(1):

    for i, (data, label0, weight, rescale, procIdx) in enumerate(tqdm(testLoader, desc='epoch %d/%d' % (epoch+1, 1))):
        data = data
        label = label0

        data = data.view(-1,192//2,2,248) ## need to change at different model
        
        output1 = fea512(data)
        
        output1 = output1.flatten(start_dim=1)
        
        output = fc1024t512(output1)
        
        out = np.concatenate([out, np.array(output.detach())])
        lab = np.concatenate([lab, np.array(label.detach())])


## draw TSNE
from sklearn.manifold import TSNE

tsne = TSNE(n_components=2)
y = tsne.fit_transform(out)
for i in range(len(y)):
    if lab[i] == 0:
        plt.plot(y[i][0], y[i][1], '.b')
    else:
        plt.plot(y[i][0], y[i][1], '.r')


## draw TSNE just label 1
for i in range(len(y)):
    if lab[i] == 1:
        plt.plot(y[i][0], y[i][1], '.r')
    else:
        continue


## draw TSNE just label 0
for i in range(len(y)):
    if lab[i] == 0:
        plt.plot(y[i][0], y[i][1], '.b')
    else:
        continue

## draw PCA
from sklearn.decomposition import PCA
pca = PCA(n_components=2)
pca.fit(out)
x_pca = pca.transform(out)
for i in range(len(x_pca)):
    if lab[i] == 1:
        plt.plot(x_pca[i][0], x_pca[i][1], '.r')
    else:
        plt.plot(x_pca[i][0], x_pca[i][1],'.b')

## draw PCA just label 1
for i in range(len(x_pca)):
    if lab[i] == 1:
        plt.plot(x_pca[i][0], x_pca[i][1], '.r')
    else:
        continue


## draw PCA just label 1
for i in range(len(x_pca)):
    if lab[i] == 0:
        plt.plot(x_pca[i][0], x_pca[i][1], '.b')
    else:
        continue

