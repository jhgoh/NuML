#!/usr/bin/env python
import sys, os
import argparse

import numpy as np
import pandas as pd
import uproot
import csv, yaml

import h5py
import torch
import torch.nn as nn

parser = argparse.ArgumentParser()
parser.add_argument('--config', action='store', type=str, default='config.yaml', help='Configration file with sample information')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='Path to output file')
parser.add_argument('-t', '--train', action='store', type=str, required=True, help='Path to training results directory')
parser.add_argument('-a', '--all', action='store_true', help='use all events for the evaluation, no split')

parser.add_argument('--device', action='store', type=int, default=0, help='device name')
parser.add_argument('--batch', action='store', type=int, default=256, help='Batch size')
parser.add_argument('--seed', action='store', type=int, default=12345, help='random seed')
args = parser.parse_args()

config = yaml.load(open(args.config).read(), Loader=yaml.FullLoader)
if args.seed: config['training']['randomSeed1'] = args.seed

sys.path.append("./python")

torch.set_num_threads(os.cpu_count())
if torch.cuda.is_available() and args.device >= 0: torch.cuda.set_device(args.device)

##### Define dataset instance #####
from dataset.WFChDataset import *
##### Define dataset instance #####
dset = WFChDataset(channel=config['format']['channel'])
for sampleInfo in config['samples']:
    if 'ignore' in sampleInfo and sampleInfo['ignore']: continue
    name = sampleInfo['name']
    dset.addSample(name, sampleInfo['path'], weight=sampleInfo['xsec']/sampleInfo['ngen'])
    dset.setProcessLabel(name, sampleInfo['label'])
dset.initialize()
lengths = [int(x*len(dset)) for x in config['training']['splitFractions']]
lengths.append(len(dset)-sum(lengths))
torch.manual_seed(config['training']['randomSeed1'])
kwargs = {'num_workers':min(config['training']['nDataLoaders'], os.cpu_count()),
          'batch_size':args.batch, 'pin_memory':False}
from torch.utils.data import DataLoader
if args.all:
    testLoader = DataLoader(dset, **kwargs)
else:
    trnDset, valDset, testDset = torch.utils.data.random_split(dset, lengths)
    #testLoader = DataLoader(trnDset, **kwargs)
    #testLoader = DataLoader(valDset, **kwargs)
    testLoader = DataLoader(testDset, **kwargs)
torch.manual_seed(torch.initial_seed())

##### Define model instance #####
from models.allModels import *
#model = WF1DCNNModel(nChannel=dset.nCh, nPoint=dset.nPt)
model = torch.load(args.train+'/model.pth', map_location='cpu')
model.load_state_dict(torch.load(args.train+'/weight.pth', map_location='cpu'))
model.fc.add_module('output', torch.nn.Sigmoid())

device = 'cpu'
if args.device >= 0 and torch.cuda.is_available():
    model = model.cuda()
    device = 'cuda'

##### Start evaluation #####
from tqdm import tqdm
labels, preds = [], []
weights = []
model.eval()
val_loss, val_acc = 0., 0.
for i, (data, label0, weight, rescale, procIdx) in enumerate(tqdm(testLoader)):
    data = data.to(device)
    label = label0.to(device)
    label0 = label0.reshape(-1)[()].numpy()
    rescale = rescale.float().to(device)
    weight = weight.float().to(device)*rescale

    pred = model(data)

    #weight = np.ones(len(label0))
    #weight[label0==1] = dset.classWeight.item()

    labels.extend([x.item() for x in label])
    weights.extend([x.item() for x in weight])
    preds.extend([x.item() for x in pred.view(-1)])

df = pd.DataFrame({'label':labels, 'prediction':preds, 'weight':weights})

fPred = args.output
df.to_csv(fPred, index=False)

##### Draw ROC curve #####
from sklearn.metrics import roc_curve, roc_auc_score
df = pd.read_csv(fPred)
tpr, fpr, thr = roc_curve(df['label'], df['prediction'], sample_weight=df['weight'], pos_label=0)
auc = roc_auc_score(df['label'], df['prediction'], sample_weight=df['weight'])

import matplotlib.pyplot as plt
df_bkg = df[df.label==0]
df_sig = df[df.label==1]

hbkg1 = df_bkg['prediction'].plot(kind='hist', histtype='step', weights=df_bkg['weight'], bins=np.linspace(0, 1, 50), alpha=0.7, color='red', label='Fast Neutron')
hsig1 = df_sig['prediction'].plot(kind='hist', histtype='step', weights=df_sig['weight'], bins=np.linspace(0, 1, 50), alpha=0.7, color='blue', label='Michel electrons')
plt.yscale('log')
plt.ylabel('Events')
plt.legend()
plt.show()

plt.plot(fpr, tpr, '.-', label='%s %.3f' % (args.train, auc))
plt.xlabel('FN efficiency')
plt.ylabel('ME efficiency')
#plt.xlim(0, 0.001)
plt.xlim(0, 1.000)
plt.ylim(0, 1.000)
plt.legend()
plt.grid()
plt.show()

