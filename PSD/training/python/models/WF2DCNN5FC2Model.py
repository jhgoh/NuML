#!/usr/bin/env python
import torch.nn as nn

class WF2DCNN5FC2Model(nn.Module):
    def __init__(self, **kwargs):
        super(WF2DCNN5FC2Model, self).__init__()

        self.nPt = kwargs['nPoint']
        self.nCh = 1 if 'nChannel' not in kwargs else kwargs['nChannel']

        nPt = self.nPt
        nH = 2
        kernel1 = 8 if 'kernel_size' not in kwargs else kwargs['kernel_size']

        self.conv1 = nn.Sequential(
            nn.Conv2d(self.nCh//2, 64, kernel_size=(2,kernel1), padding=(1,0)),
            nn.MaxPool2d((1,kernel1)),
            nn.ReLU(),
            nn.BatchNorm2d(64),
            nn.Dropout(0.5),
        )
        nPt = (nPt-kernel1+1)//kernel1
        nH += 1

        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=(2,3), padding=(1,0)),
            nn.MaxPool2d((1,2)),
            nn.ReLU(),
            nn.BatchNorm2d(128),
            nn.Dropout(0.5),
        )
        nPt = (nPt-3+1)//2
        nH += 1

        self.conv3 = nn.Sequential(
            nn.Conv2d(128, 256, kernel_size=(2,3), padding=(1,0)),
            nn.MaxPool2d((1,2)),
            nn.ReLU(),
            nn.BatchNorm2d(256),
            nn.Dropout(0.5),
        )
        nPt = (nPt-3+1)//2
        nH += 1

        self.conv4 = nn.Sequential(
            nn.Conv2d(256, 512, kernel_size=(2,3), padding=(1,0)),
            nn.MaxPool2d((1,2)),
            nn.ReLU(),
            nn.BatchNorm2d(512),
            nn.Dropout(0.5),
        )
        nPt = (nPt-3+1)//2
        nH += 1

        self.conv5 = nn.Sequential(
            nn.Conv2d(512, 1024, kernel_size=(2,3), padding=(1,0)),
            nn.MaxPool2d((1,2)),
            nn.ReLU(),
            nn.BatchNorm2d(1024),
            nn.Dropout(0.5),
        )
        nPt = (nPt-3+1)//2
        nH += 1

        nPt = nPt*nH

        self.fc = nn.Sequential(
            nn.Linear(nPt*1024, 512),
            nn.ReLU(), nn.Dropout(0.5),

            nn.Linear(512, 512),
            nn.ReLU(), nn.Dropout(0.5),

            nn.Linear(512, 1),
        )

    def forward(self, x):
        batch, c, n = x.shape
        #assert (n == self.nPt)
        #assert (c == self.nCh)

        x = x.view(-1,c//2,2,n)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)

        x = x.flatten(start_dim=1)
        x = self.fc(x)

        return x

