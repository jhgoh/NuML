#!/usr/bin/env python
import sys, os, argparse, uproot
import numpy as np, pandas as pd, xgboost as xgb

nP = 2480
window = 5*16

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', action='store', type=str, required=True, help='Path to input file')
parser.add_argument('-m', '--model', action='store', type=str, required=True, help='pretrained model')
parser.add_argument('-a', '--all', action='store_true', help='use all events for the evaluation, no split')
args = parser.parse_args()

varNames = []
df = pd.DataFrame()

fin = uproot.open(args.input)
df['labels'] = np.array(fin["tree/label"].array().tolist())
for i in range(2480//(5*16)):
    varName = 'wf%d'%i
    varNames.append(varName)
    df[varName] = np.array(fin["tree/"+varName].array().tolist())

if args.all:
    testDataset = df
else:
    trnDataset = df.sample(frac=0.6, random_state=12345)
    valDataset = df.drop(trnDataset.index)
    testDataset = valDataset.sample(frac=0.5, random_state=12345)
    valDataset = valDataset.drop(testDataset.index)

testSigDataset = testDataset.loc[testDataset.labels==1]
testBkgDataset = testDataset.loc[testDataset.labels==0]

#trnDataset = xgb.DMatrix(data=trnDataset[varNames], label=trnDataset['labels'])
#valDataset = xgb.DMatrix(data=valDataset[varNames], label=valDataset['labels'])
testDMatrix = xgb.DMatrix(data=testDataset[varNames], label=testDataset['labels'])
testSigDMatrix = xgb.DMatrix(data=testSigDataset[varNames], label=testSigDataset['labels'])
testBkgDMatrix = xgb.DMatrix(data=testBkgDataset[varNames], label=testBkgDataset['labels'])

model = xgb.Booster()
model.load_model(args.model)

pred = model.predict(testDMatrix)
predSig = model.predict(testSigDMatrix)
predBkg = model.predict(testBkgDMatrix)

import matplotlib.pyplot as plt
plt.hist(predBkg, bins=np.linspace(0,1,50), histtype='step', color='red', density=True)
plt.hist(predSig, bins=np.linspace(0,1,50), histtype='step', color='blue', density=True)
#plt.yscale('log')
plt.show()

from sklearn.metrics import roc_curve, roc_auc_score
fpr, tpr, thr = roc_curve(testDataset['labels'], model.predict(testDMatrix))
auc = roc_auc_score(testDataset['labels'], model.predict(testDMatrix))
plt.plot(tpr, 1-fpr, '.-', label='AUC=%.3f' % (auc))
#plt.xlabel('FN efficiency')
#plt.ylabel('ME efficiency')
#plt.xlim(0, 0.001)
plt.xlim(0, 1.000)
plt.ylim(0, 1.000)
plt.legend()
plt.grid()
plt.show()

