#!/usr/bin/env python
import sys, os
import argparse

import numpy as np
import uproot
import csv

import h5py
import torch
import torch.nn as nn

sys.path.append("./python")
from dataset.WFDataset import *
from models.allModels import *

parser = argparse.ArgumentParser()
parser.add_argument('--epoch', action='store', type=int, default=100, help='Number of epochs')
parser.add_argument('--batch', action='store', type=int, default=32, help='Batch size')
parser.add_argument('-i', '--input', action='store', type=str, required=True, help='Path to input file')
parser.add_argument('-o', '--output', action='store', type=str, required=True, help='Path to output directory')
parser.add_argument('--lr', action='store', type=float, default=1e-4, help='Learning rate')
parser.add_argument('--device', action='store', type=int, default=0, help='device name')
parser.add_argument('--seed', action='store', type=int, default=12345, help='random seed')
parser.add_argument('--channel', choices=[1,96], default=1, type=int, help='number of channels')
parser.add_argument('--kernel_size', action='store', type=int, default=3, help='kernel size at the 1st layer')
models = ['CNN1FC1', 'CNN1FC2', 'CNN3FC1', 'CNN3FC2', 'CNN5FC1', 'CNN5FC2']
parser.add_argument('--model', choices=models, default=models[0], help='model name')
args = parser.parse_args()

torch.set_num_threads(os.cpu_count())
if torch.cuda.is_available() and args.device >= 0: torch.cuda.set_device(args.device)
if not os.path.exists(args.output): os.makedirs(args.output)

##### Define dataset instance #####
dset = WFDataset(args.input, channel=args.channel)
lengths = [int(0.6*len(dset)), int(0.2*len(dset))]
lengths.append(len(dset)-sum(lengths))
torch.manual_seed(args.seed)
trnDset, valDset, testDset = torch.utils.data.random_split(dset, lengths)
kwargs = {'num_workers':0, 'pin_memory':True}
from torch.utils.data import DataLoader
trnLoader = DataLoader(trnDset, batch_size=args.batch, shuffle=True, **kwargs)
valLoader = DataLoader(valDset, batch_size=args.batch, shuffle=False, **kwargs)
torch.manual_seed(torch.initial_seed())

##### Define model instance #####
#model = WF1DCNNModel(nChannel=dset.nCh, nPoint=dset.nPt)
#model = WF1DCNN3LayerModel(nChannel=dset.nCh, nPoint=dset.nPt, kernel_size=args.kernel_size)
#model = WF1DCNN3FC2Model(nChannel=dset.nCh, nPoint=dset.nPt, kernel_size=args.kernel_size)
exec('model = WF1D'+args.model+'Model(nChannel=dset.nCh, nPoint=dset.nPt, kernel_size=args.kernel_size)')
torch.save(model, os.path.join(args.output, 'model.pth'))

device = 'cpu'
if args.device >= 0 and torch.cuda.is_available():
    model = model.cuda()
    device = 'cuda'

##### Define optimizer instance #####
optm = torch.optim.Adam(model.parameters(), lr=args.lr)
crit = torch.nn.BCEWithLogitsLoss(pos_weight=dset.classWeight)
if device == 'cuda': crit = crit.cuda()

##### Start training #####
with open(args.output+'/summary.txt', 'w') as fout:
    fout.write(str(args))
    fout.write('\n\n')
    fout.write(str(model))
    fout.close()

from sklearn.metrics import accuracy_score
from tqdm import tqdm
bestState, bestLoss = {}, 1e9
train = {'loss':[], 'acc':[], 'val_loss':[], 'val_acc':[]}
for epoch in range(args.epoch):
    model.train()
    trn_loss, trn_acc = 0., 0.
    optm.zero_grad()
    for i, (data, label0) in enumerate(tqdm(trnLoader, desc='epoch %d/%d' % (epoch+1, args.epoch))):
        data = data.to(device)
        label = label0.to(device)
        label0 = label0.reshape(-1).numpy()

        pred = model(data)
        l = crit(pred, label)
        l.backward()

        optm.step()
        optm.zero_grad()

        weight = np.ones(len(label))
        weight[label0==1] = dset.classWeight.item()
        label = label.to(device)

        trn_loss += l.item()
        trn_acc += accuracy_score(label0, np.where(pred.to('cpu') > 0.5, 1, 0), sample_weight=weight)

    trn_loss /= len(trnLoader)
    trn_acc  /= len(trnLoader)

    model.eval()
    val_loss, val_acc = 0., 0.
    for i, (data, label0) in enumerate(tqdm(valLoader)):
        data = data.to(device)
        label = label0.to(device)
        label0 = label0.reshape(-1).numpy()

        pred = model(data)
        loss = crit(pred, label)

        weight = np.ones(len(label))
        weight[label0==1] = dset.classWeight.item()

        if torch.isnan(loss):
            print("nan in validation loss", loss.item())
            continue
        val_loss += loss.item()
        val_acc += accuracy_score(label0, np.where(pred.to('cpu') > 0.5, 1, 0), sample_weight=weight)
    val_loss /= len(valLoader)
    val_acc  /= len(valLoader)

    if bestLoss > val_loss:
        bestState = model.to('cpu').state_dict()
        bestLoss = val_loss
        torch.save(bestState, os.path.join(args.output, 'weight.pth'))

        model.to(device)

    train['loss'].append(trn_loss)
    train['acc'].append(trn_acc)
    train['val_loss'].append(val_loss)
    train['val_acc'].append(val_acc)

    with open(os.path.join(args.output, 'train.csv'), 'w') as f:
        writer = csv.writer(f)
        keys = train.keys()
        writer.writerow(keys)
        for row in zip(*[train[key] for key in keys]):
            writer.writerow(row)

bestState = model.to('cpu').state_dict()
torch.save(bestState, os.path.join(args.output, 'weightFinal.pth'))


