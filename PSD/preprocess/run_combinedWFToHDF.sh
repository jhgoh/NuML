#!/bin/bash

if [ $# -le 0 ]; then
    echo "Usage: $0 INPUT_DIRECTORY RECO_DIRECTORY OUTPUT_DIRECTORY"
    exit 0
fi

DATADIR=$1
RECODIR=$2
OUTDIR=$3
NPROC=`nproc`
[ $NPROC -gt 16 ] && NPROC=16

if [ ! -d $DATADIR ]; then
    echo "Cannot find directory with combined waveform data"
    exit 1
fi
if [ ! -d $RECODIR ]; then
    echo "Cannot find directory with reconstruction data"
    exit 1
fi
if [ -d $OUTDIR ]; then
    echo "Output directory already exists. Stop processing"
    exit 2
fi

mkdir -p $OUTDIR

NDATA=`ls -U1 $DATADIR | wc -l`
IDATA=0
for DATA in `ls -U1 $DATADIR`; do
    IDATA=$((IDATA+1))
    #echo -ne "Processing $IDATA/$NDATA...\r"

    SUFFIX=`echo $DATA | sed -e 's;comb.;;g' -e 's;.root$;;g'`
    if [ ! -f $DATADIR/comb.$SUFFIX.root ]; then
        echo "Cannot find combined waveform root file" comb.$SUFFIX.root
        continue
    fi
    if [ ! -f $RECODIR/reco.$SUFFIX.root ]; then
        echo "Cannot find reconstruction root file" reco.$SUFFIX.root
        continue
    fi

    #`dirname $0`/combinedWFToHDF.py -q -i $DATADIR/comb.$SUFFIX.root \
    #                                   -r $RECODIR/reco.$SUFFIX.root \
    #                                   -o $OUTDIR/$SUFFIX.h5
    echo "-q -i $DATADIR/comb.$SUFFIX.root \
             -r $RECODIR/reco.$SUFFIX.root \
             -o $OUTDIR/$SUFFIX.h5"
done | xargs -L1 -P$NPROC `dirname $0`/combinedWFToHDF.py
echo ""
