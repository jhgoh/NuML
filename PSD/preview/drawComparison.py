#!/usr/bin/env python
import sys, os
import ROOT

sys.path.append("../macros")

ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
#ROOT.EnableThreadSafety()
#ROOT.EnableImplicitMT(200)

proof = ROOT.TProof.Open("workers=64")
chain = ROOT.TChain("prod")
chain.SetProof()
for fName in sys.argv[1:]:
    if not fName.endswith(".root"): continue
    chain.Add(fName)

weight_FN = "1/(%f*recoVertexArray[2] + %f)" % (1.17350e-02, 1.58633e-02)
weight_ME = "1/(%f*recoVertexArray[2] + %f)" % (-1.62927e-03, 1.66471e-02)
cut0 = "abs(recoVertexArray[2])<0.6 && hypot(recoVertexArray[0], recoVertexArray[1])<1.0"
#cutCommon = "Charge!=0 && abs(recoVertexArray[2])<0.6 && hypot(recoVertexArray[0], recoVertexArray[1])<0.6"
cutFN = "MEFlag==0&&FNFlag==1"
cutME = "MEFlag==1&&FNFlag==0"

hQtQ_ME = ROOT.TH1D("hQtQ_ME", "", 200, -2, 2)
hQtQ_FN = ROOT.TH1D("hQtQ_FN", "", 200, -2, 2)

hVtxZ_ME = ROOT.TH1D("hVtxZ_ME", "", 150, -1.5, 1.5)
hVtxZ_FN = ROOT.TH1D("hVtxZ_FN", "", 150, -1.5, 1.5)

hVtxR_ME = ROOT.TH1D("hVtxR_ME", "", 100, 0, 2)
hVtxR_FN = ROOT.TH1D("hVtxR_FN", "", 100, 0, 2)

hVtxRZ_ME = ROOT.TH2D("hVtxRZ_ME", "", 50, 0, 2, 50, -1.5, 1.8)
hVtxRZ_FN = ROOT.TH2D("hVtxRZ_FN", "", 50, 0, 2, 50, -1.5, 1.8)

chain.Draw("ChargeTail/Charge>>hQtQ_ME", "(%s)*(%s)" % (weight_ME, "&&".join([cut0, cutME])), "goff")
chain.Draw("ChargeTail/Charge>>hQtQ_FN", "(%s)*(%s)" % (weight_FN, "&&".join([cut0, cutFN])), "goff")
chain.Draw("recoVertexArray[2]>>hVtxZ_ME", "(%s)*(%s)" % (weight_ME, "&&".join([cut0, cutME])), "goff")
chain.Draw("recoVertexArray[2]>>hVtxZ_FN", "(%s)*(%s)" % (weight_FN, "&&".join([cut0, cutFN])), "goff")
chain.Draw("hypot(recoVertexArray[0],recoVertexArray[1])>>hVtxR_ME", "(%s)*(%s)" % (weight_ME, "&&".join([cut0, cutME])), "goff")
chain.Draw("hypot(recoVertexArray[0],recoVertexArray[1])>>hVtxR_FN", "(%s)*(%s)" % (weight_FN, "&&".join([cut0, cutFN])), "goff")
chain.Draw("recoVertexArray[2]:hypot(recoVertexArray[0],recoVertexArray[1])>>hVtxRZ_ME", "(%s)*(%s)" % (weight_ME, "&&".join([cut0, cutME])), "goff")
chain.Draw("recoVertexArray[2]:hypot(recoVertexArray[0],recoVertexArray[1])>>hVtxRZ_FN", "(%s)*(%s)" % (weight_FN, "&&".join([cut0, cutFN])), "goff")

for h in [hQtQ_ME, hVtxZ_ME, hVtxR_ME]:
    h.SetLineColor(ROOT.kRed+1)
    if h.Integral() > 0: h.Scale(1./h.Integral())
for h in [hQtQ_FN, hVtxZ_FN, hVtxR_FN]:
    h.SetLineColor(ROOT.kBlue+1)
    if h.Integral() > 0: h.Scale(1./h.Integral())
for h in [hVtxRZ_ME, hVtxRZ_FN]:
    if h.Integral() > 0: h.Scale(1./h.Integral())

## Apply plotting style
from PrettyPlot import PrettyPlot

plotQtQ = PrettyPlot(configToOverride={'header/image':None})
plotQtQ.drawOverlay('QtQ', hists=[hQtQ_ME, hQtQ_FN],
                     titles=["Michel #scale[1.2]{#font[12]{e}}", "Fast #scale[1.2]{#font[12]{n}}",],
                     config={'header/label':"JSNS^{2} Work in progress",
                             'x-axis/title':'Q_{tail}/Q_{total}', 'y-axis/title':'Arbitrary unit'})
plotVtxZ = PrettyPlot(configToOverride={'header/image':None})
plotVtxZ.drawOverlay('VtxZ', hists=[hVtxZ_ME, hVtxZ_FN],
                      titles=["Michel #scale[1.2]{#font[12]{e}}", "Fast #scale[1.2]{#font[12]{n}}"],
                      config={'header/label':"JSNS^{2} Work in progress",
                              'x-axis/title':'Vertex Z (m)', 'y-axis/title':'Arbitrary unit'})

plotVtxR = PrettyPlot(configToOverride={'header/image':None})
plotVtxR.drawOverlay('VtxR', hists=[hVtxR_ME, hVtxR_FN],
                      titles=["Michel #scale[1.2]{#font[12]{e}}", "Fast #scale[1.2]{#font[12]{n}}"],
                      config={'header/label':"JSNS^{2} Work in progress",
                              'x-axis/title':'Vertex R (m)', 'y-axis/title':'Arbitrary unit'})

plotVtxRZ = PrettyPlot(configToOverride={'header/image':None, 'canvas/margin-right':80})
plotVtxRZ.drawSideBySide('VtxRZ', hists=[hVtxRZ_ME, hVtxRZ_FN],
                          titles=["Michel #scale[1.2]{#font[12]{e}}", "Fast #scale[1.2]{#font[12]{n}}"],
                          config={'header/label':"JSNS^{2} Work in progress",
                                  'x-axis/title':'Vertex R (m)', 'y-axis/title':'Vertex Z (m)',
                                  'plot/option':"colz"})

