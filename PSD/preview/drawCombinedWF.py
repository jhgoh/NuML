#!/usr/bin/env python
import sys, os, time
import argparse

parser = argparse.ArgumentParser(description='Convert pulse shape root files to hdf')
parser.add_argument('inputFiles', metavar='N', type=str, nargs='+',
                    help='Input file names or directory')
parser.add_argument('-q', '--quiet', action='store_true', default=False, help='Supress verbose output')
args = parser.parse_args()

from glob import glob
fNames = []
for d in args.inputFiles:
    if d.endswith('.root'): fNames.append(d)
    elif os.path.isdir(d): fNames.extend(glob(d+'/*.root'))

nPdstlT = 35
nCh, nT = 0, 0
import uproot
import numpy as np
import tqdm
subRuns, trigIDs = [], []
waveforms = []
dTs, dRs = [], []
for fName in (fNames if args.quiet else tqdm.tqdm(fNames)):
    fin = uproot.open(fName)
    tree = fin['comTree']

    waveform = np.array(tree['mVcomspAlign'].array())
    if nT == 0:
        nCh = waveform.shape[-2]
        nT = waveform.shape[-1]
    if waveform.shape[1:] != (nCh, nT):
        print("!!! Inconsistent array shape, ", waveform.shape)
        break

    waveforms.append(waveform)

    dTs.append(tree['dT'].array())
    dRs.append(tree['dVertex'].array())

    subRun = 0 if 'debug.r' not in fName else fName.split('.')[-2][1:].lstrip('0')
    subRun = 0 if subRun == '' else int(subRun)
    subRuns.append(np.ones(waveform.shape[0])*subRun)
    trigIDs.append(tree['TrigID'].array())

## Just to avoid confusion due to typo, remove temoprary variables in the loop
del(waveform)
del(subRun)

## shape: (nEvent, nPMT, nTime)
waveforms = np.concatenate(waveforms, axis=0)
dTs = np.concatenate(dTs)
dRs = np.concatenate(dRs)
subRuns = np.concatenate(subRuns)
trigIDs = np.concatenate(trigIDs)

if not args.quiet:
    print("waveforms : shape=", waveforms.shape)
    print("              min=", waveforms.min(), "max=", waveforms.max())

import matplotlib.pyplot as plt
plt.hist(dTs/1000, bins=np.linspace(0, 10, 100))
plt.xlabel('$\Delta$T (ms)')
plt.ylabel('Events per 100$\mu$s')
plt.show()
plt.hist(dRs, bins=np.linspace(0, 1.5, 100))
plt.xlabel('$\Delta$R (m)')
plt.ylabel('Events per 1.5cm')
plt.show()

fig = plt.figure(figsize=(10,7))
fig.show()

for i in range(waveforms.shape[0]):
    ax0 = plt.subplot2grid((3,3), (0,0), colspan=2, rowspan=3)
    ax1 = plt.subplot2grid((3,3), (0,2))
    ax2 = plt.subplot2grid((3,3), (1,2))
    ax3 = plt.subplot2grid((3,3), (2,2))

    ax0.set_xlabel('time ($\mu$s)')
    ax0.set_ylabel('Q per 2$\mu$s')

    ax1.set_xlabel('q before %.0f$\mu$s' % (2*nPdstlT))
    ax2.set_xlabel('q after %.0f$\mu$s' % (2*nPdstlT))
    ax3.set_xlabel('PMT total Q fraction')

    for k in range(nCh):
        p = k%12
        c = k//12

        ## Compute pedestal
        pdstl = np.mean(waveforms[i,k,:nPdstlT])

        ## x values and y values, scaled by each PMT channel's gains
        t = np.linspace(0, nT*2, nT)
        y = waveforms[i,k]#-pdstl

        ## Draw guidelines
        #ax1.plot([0, nT*2], [0, 0], linewidth=.7, color='tab:gray')
        ax0.plot([0, nT*2], [pdstl, pdstl], linewidth=.7, color='tab:gray')
        ax0.plot([2*nPdstlT,2*nPdstlT], [y.min(), y.max()], linewidth=.7, color='tab:gray')

        ## Draw the lineshapes
        ax0.plot(t, y, drawstyle='steps-pre', linewidth=1.)

    ax1.hist(waveforms[i,:,:35].reshape(-1), bins=np.linspace(-10,10,50), color='tab:gray')
    ax2.hist(waveforms[i,:,35:].reshape(-1), bins=np.linspace(-10,1500,50))
    ax3.hist(np.sum(waveforms[i,:,35:], axis=1)/np.sum(waveforms[i]), bins=np.linspace(0, 1, 100))
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    ax3.set_yscale('log')

    plt.tight_layout()
    fig.canvas.draw()
    fig.canvas.flush_events()

    time.sleep(1)
