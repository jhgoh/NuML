#!/usr/bin/env python
import pandas as pd
from tqdm import tqdm
from glob import glob

dfs = {}

psdName = "PSD_Convention"
dfs[psdName] = []
fNames = glob("/store/hep/data/JSNS2/PSDscore/ConventionalPSD_shjeon/output/FN/psd*.txt")
for fName in tqdm(fNames):
    df = pd.read_csv(fName, sep='\t', index_col=False)
    df['label'] = 0.0
    dfs[psdName].append(df)
fNames = glob("/store/hep/data/JSNS2/PSDscore/ConventionalPSD_shjeon/output/ME/psd*.txt")
for fName in tqdm(fNames):
    df = pd.read_csv(fName, sep='\t', index_col=False)
    df['label'] = 1.0
    dfs[psdName].append(df)
dfs[psdName] = pd.concat(dfs[psdName])
dfs[psdName].rename(columns={'Run': 'run', 'SubRun':'subrun', 'TrigID':'trigID', 'PSD':psdName}, inplace=True)
dfs[psdName]['idx'] = dfs[psdName]['run']*10000*100000+dfs[psdName]['subrun']*100000+dfs[psdName]['trigID']
dfs[psdName].set_index(['idx'])
#print('PSD with conventional digital sum method (SKKU.SH)')
#print(dfs[psdName])

psdName = "PSD_ChargeL"
dfs[psdName] = []
df = pd.read_csv("/store/hep/data/JSNS2/PSDscore/result_210425_tmaru/ME_ln.dat", names=["subrun", "trigID", psdName], sep=' ', index_col=False)
df['run'] = 1563
df['label'] = 1.0
dfs[psdName].append(df)
df = pd.read_csv("/store/hep/data/JSNS2/PSDscore/result_210425_tmaru/FN_ln.dat", names=["subrun", "trigID", psdName, "DVTX"], sep=' ', index_col=False)
df['run'] = 1563
df['label'] = 0.0
dfs[psdName].append(df)
dfs[psdName] = pd.concat(dfs[psdName])
dfs[psdName]['idx'] = dfs[psdName]['run']*10000*100000+dfs[psdName]['subrun']*100000+dfs[psdName]['trigID']
dfs[psdName].set_index(['idx'])
#print('PSD with charge likelihood (KEK.TM)')
#print(dfs[psdName])

psdName = 'PSD_NN'
dfs[psdName] = pd.read_csv("/store/hep/data/JSNS2/PSDscore/run1563_CNN_PSD_testDataset_changhyun.csv", index_col=False)
dfs[psdName].rename(columns={'TrigID': 'trigID', 'pred':psdName}, inplace=True)
dfs[psdName]['idx'] = dfs[psdName]['run']*10000*100000+dfs[psdName]['subrun']*100000+dfs[psdName]['trigID']
dfs[psdName].set_index(['idx'])
#print("PSD with CNN (KHU.CH)")
#print(dfs[psdName])

psdName = 'PSD_ShapeLR'
dfs[psdName] = []
df = pd.read_csv("/store/hep/data/JSNS2/PSDscore/data_likelihood_hwang/ME__PDF_bin248.csv")
df['label'] = 1.0
dfs[psdName].append(df)
df = pd.read_csv("/store/hep/data/JSNS2/PSDscore/data_likelihood_hwang/FN__PDF_bin248.csv")
df['label'] = 0.0
dfs[psdName].append(df)
dfs[psdName] = pd.concat(dfs[psdName])
dfs[psdName].rename(columns={'TrigID': 'trigID', 'LikelihoodRatio':psdName}, inplace=True)
dfs[psdName]['idx'] = dfs[psdName]['run']*10000*100000+dfs[psdName]['subrun']*100000+dfs[psdName]['trigID']
dfs[psdName].set_index(['idx'])
#print("PSD with Shape likelihood (KHU.WS)")
#print(dfs[psdName])

import matplotlib as mpl
import matplotlib.pyplot as plt
#psd1, psd2 = 'PSD_Convention', 'PSD_ChargeL'
psd1, psd2 = 'PSD_Convention', 'PSD_NN'
#psd1, psd2 = 'PSD_Convention', 'PSD_ShapeLR'
#psd1, psd2 = 'PSD_ChargeL', 'PSD_NN'
#psd1, psd2 = 'PSD_ChargeL', 'PSD_ShapeLR'
#psd1, psd2 = 'PSD_ShapeLR', 'PSD_NN'
df = pd.merge(dfs[psd1], dfs[psd2][['idx', psd2]], how='inner', left_on='idx', right_on='idx')
df_ME = df.query('label == 1')
df_FN = df.query('label == 0')

from sklearn.metrics import roc_curve, auc
from sklearn.metrics import roc_auc_score
sgn1 = -1 if psd1 == 'PSD_Convention' else 1
sgn2 = -1 if psd2 == 'PSD_Convention' else 1
fpr1, tpr1, _ = roc_curve(df['label'], sgn1*df[psd1])
fpr2, tpr2, _ = roc_curve(df['label'], sgn2*df[psd2])
auc1 = auc(fpr1, tpr1)
auc2 = auc(fpr2, tpr2)

fig, ax = plt.subplots(2,3, figsize=(12,6))
ax[0,0].plot(df_ME[psd1], df_ME[psd2], 'ob', markersize=1, label='ME')
ax[0,0].plot(df_FN[psd1], df_FN[psd2], 'or', markersize=1, label='FN')
kwargs = {'density':True, 'histtype':'step', 'bins':50}
ax[0,1].hist(df_ME[psd1], color='darkblue', label='ME', **kwargs)
ax[0,1].hist(df_FN[psd1], color='darkred', label='FN', **kwargs)
ax[0,2].hist(df_ME[psd2], color='darkblue', label='ME', **kwargs)
ax[0,2].hist(df_FN[psd2], color='darkred', label='FN', **kwargs)
ax[1,0].hist2d(df_ME[psd1], df_ME[psd2], bins=50, cmap='jet', norm=mpl.colors.LogNorm(), label='ME')
ax[1,1].hist2d(df_FN[psd1], df_FN[psd2], bins=50, cmap='jet', norm=mpl.colors.LogNorm(), label='FN')
ax[1,2].plot(fpr1, tpr1, label=(psd1+(' AUC=%.3f'%auc1)))
ax[1,2].plot(fpr2, tpr2, label=(psd2+(' AUC=%.3f'%auc2)))
#ax[0,0].set_title('PSD comparisions')
#ax[1,0].set_title('Michel electrons')
#ax[1,1].set_title('Fast neutrons')
ax[0,1].set_xlabel(psd1)
ax[0,2].set_xlabel(psd2)
for a in (ax[0,0], ax[1,0], ax[1,1]):
    a.set_xlabel(psd1)
    a.set_ylabel(psd2)
for a in ax.reshape(-1):
    a.legend()
plt.tight_layout()
plt.show()
