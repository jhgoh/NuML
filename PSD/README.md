# Pulse Shape Discrimination with Machine Learning

## preprocessing: Preprecessing for the JSNS2 ML-PSD
This directory contains set of macros to run the preprocessing step of the 
ML-based PSD development for the JSNS2 experiment.

Data produced from the JSNS2 experiment are given with the ROOT format, but it
is not that friendly format for the usual ML tools at this moment. The HDF5 
format is widely used in the ML community and fit with the numpy ndarray which
is the native format for the ML. Therefore, we introduce this preprocessing 
step to convert ROOT to HDF.

Currently, 

**Macros files**
- rawRecoToHDF.py
- nPulseShapesToHDF.py
- pulseShapeToHDF.py


