#!/bin/bash

#INPUTFILE=/users/yewzzang/rat/RAT/rat-pac-jsns2-v2.0/mac/jsns2/test_pos_1.root
[ -z $1 ] && INPUTFILE=../simulate/output_1.root || INPUTFILE=$1

INPUTFILE=`readlink -f $INPUTFILE`
INPUTDIR=`dirname $INPUTFILE`
singularity run -B$INPUTDIR:$INPUTDIR /store/sw/singularity/jsns2/jsns2-jade0-20221212.sif <<EOF
make || exit
./extractMCEvent $INPUTFILE mcEvent.csv
./extractMCPMT $INPUTFILE mcPMT.csv
EOF
