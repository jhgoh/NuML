#!/bin/bash

SIF=/store/sw/singularity/jsns2/jsns2-jade0-20221212.sif
MACFILE=$1
[ -z $2 ] && INDEX=1 || INDEX=$2
OUTFILE=output_$INDEX.root

singularity run $SIF <<EOF
rat $1 -o $OUTFILE
EOF
